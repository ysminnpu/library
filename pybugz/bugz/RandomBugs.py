'''
See LICENCE_GPL for licensing information

@author: spd
'''
import random
from xml.etree.ElementTree import dump
from urlparse import urljoin
import bugzilla
from config import config
import cli
import webbrowser
from xml.etree import ElementTree

class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                # treat like dict
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                # treat like list
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)

class XmlDictConfig(dict):
    '''
    Example usage:

    >>> tree = ElementTree.parse('your_file.xml')
    >>> root = tree.getroot()
    >>> xmldict = XmlDictConfig(root)

    Or, if you want to use an XML string:

    >>> root = ElementTree.XML(xml_string)
    >>> xmldict = XmlDictConfig(root)
{
                                }
    And then use xmldict for what it is... a dict.
    '''
    def __init__(self, parent_element):
        childrenNames = []
        for child in parent_element.getchildren():
            childrenNames.append(child.tag)
    
        if parent_element.items(): #attributes
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                #print len(element), element[0].tag, element[1].tag
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
    
                if childrenNames.count(element.tag) > 1:
                    try:
                        currentValue = self[element.tag]
                        currentValue.append(aDict)
                        self.update({element.tag: currentValue})
                    except: #the first of its kind, an empty list must be created
                        self.update({element.tag: [aDict]}) #aDict is written in [], i.e. it will be a list
    
                else:
                    self.update({element.tag: aDict})
            # this used to assume that if you've got an attribute in a tag,
            # you won't be having any text. Now it assumes you don't have
            # an attribute called <tagname>.value
            elif element.items():
                d=dict(element.items())
                if element.text:
                    d[element.tag + '.value'] = element.text
                self.update({element.tag: d})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})
                
class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
    def __getattr__(self, name):
        value = self[name]
        if isinstance(value, dict):
            return AttrDict(value)
        return value

if __name__ == '__main__':
    min_id = 1
    max_id = 340000
    num_bugs = 10
    max_retries = 10
    url = 'https://bugs.eclipse.org/bugs/'
    
    bugz = bugzilla.Bugz(url, skip_auth=True)
    bugs = []
    num_retries = 0
    while len(bugs) < num_bugs and num_retries < max_retries:
        num_retries += 1
        id = random.randint(min_id, max_id)
        tree = bugz.get(id)
        if not tree:
            continue
        bug = AttrDict(XmlDictConfig(tree.getroot()))
        print bug
        print bug.bug.bug_status
        if bug.bug.bug_status in ['NEW']:
            continue
        
        print bug.bug.resolution
        if bug.bug.resolution in ['INVALID', 'DUPLICATE', 'WORKSFORME', 'NOT_ECLIPSE', 'WONTFIX']:
            continue
        
        print bug.bug.product
        if bug.bug.product not in ['Platform']:
            continue
        
        print 'Adding'
        bugs.append(bug)
        num_retries = 0

    for bug in bugs:
        bug_url = urljoin(url, config.urls['show']) + '?id=' + str(bug.bug.bug_id)
        webbrowser.open(bug_url, 2, False)
