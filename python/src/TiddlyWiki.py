'''
See LICENCE_BSD for licensing information

@author: spd
'''
import codecs
import os
from datetime import datetime
from xml.sax import saxutils
from BeautifulSoup import BeautifulSoup
import re
import tempfile
import shutil

def open_files(config, in_key, out_key, overwrite=False):
    in_file = open(os.path.abspath(config[in_key]))
    path = os.path.abspath(config[out_key])
    if not overwrite and os.path.exists(path):
        print path, 'already exists'
        exit()
    out_file = codecs.open(path, 'w', 'utf-8')
    return (in_file, out_file)
    
def create_tiddly_wiki(config, in_file, out_file, write_tiddlers):
    for line in in_file:
        out_file.write(line)
        if '<div id="storeArea">' in line:
            write_tiddlers(config, out_file)
    in_file.close()
    out_file.close()
    
def read_tiddlers(in_file, *args, **kwargs):
    parser = BeautifulSoup(in_file)
    return parser.findAll("div", title=True, *args, **kwargs);


def update_tiddly_wiki(config, in_file, write_tiddlers):
    tmp = tempfile.mkstemp()
    out_file = os.fdopen(tmp[0], 'w')
    tiddlers = read_tiddlers(in_file, tags=re.compile('.*preserve.*'))
    
    in_file.seek(0)
    
    writing = True
    for line in in_file:
        if '<!--POST-STOREAREA-->' in line:
            writing = True
            out_file.write('</div>\n')
        if writing:
            out_file.write(line);
        if '<div id="storeArea">' in line:
            for tiddler in tiddlers:
                out_file.write(str(tiddler))
                out_file.write('\n')
            write_tiddlers(config, out_file)
            writing = False
    in_file.close()
    out_file.close()
    
    shutil.move(tmp[1], in_file.name)
    
def parse_tags(tags):
    parsed = []
    if tags:
        escaped = None
        for token in tags.split():
            if escaped and ']]' in token:
                parsed.append(escaped + ' ' + token.replace(']]', ''))
                escaped = None
            elif escaped:
                escaped += ' ' + token
            elif '[[' in token:
                escaped = token.replace('[[', '')
            else:
                parsed.append(token)
    return parsed
    
def write_tiddler_start(out_file, title, created=datetime.now()):
    out_file.write('<div')
    write_attribute(out_file, 'title', title)
    write_attribute(out_file, 'creator', 'spd')
    write_attribute(out_file, 'modifier', 'spd')
    if created:
        write_attribute(out_file, 'created', created.strftime('%Y%m%d%H%M'))
    write_attribute(out_file, 'changecount', '1')

def write_content_start(out_file):
    out_file.write(' >\n')
    out_file.write('<pre>\n')
    
def write_tiddler_end(out_file):
    out_file.write('</pre>\n')
    out_file.write('</div>\n')
    
def write_tags(out_file, *args):
    tags = ''
    for tag in args:
        if ' ' in tag:
            tag = '[[' + tag + ']]'
        tags += ' ' + tag
    write_attribute(out_file, 'tags', tags)

def write_attribute(out_file, name, value=None):
    out_file.write(' ')
    out_file.write(name)
    out_file.write('=')
    if value:
        value = value.replace('\n', ' ')
        value = saxutils.quoteattr(value)
        out_file.write(value)
    else:
        out_file.write('""')
    
def write_image_link(out_file, img, link=None, flt=None, tooltip=None):
    out_file.write(format_image_link(img, link, flt))

def format_image_link(img, link=None, flt=None, tooltip=None):
    if link and not isinstance(link, basestring):
        link = unicode(link)
    result = '['
    if flt:
        result += flt
    result += 'img['
    if tooltip:
        result += tooltip + '|'
    result += 'resources/' + img + '.png]'
    if link:
        result += '[' + link + ']'
    result += ']'
    return result

def format_link(link, value=None):
    if value:
        return '[[' + str(value) + '|' + str(link) + ']]'
    return '[[' + str(link) + ']]'

def macro(macro):
    return '&lt;&lt;' + macro + '&gt;&gt;'

def header_row(iterable, sortable=False):
    return row(['!' + str(i) for i in iterable], sortable)

def row(iterable, sortable=False):
    result = ''
    result += '|'
    result += '|'.join(map(str, iterable))
    result += '|'
    if sortable:
        result += 'h'
    result += '\n'
    return result

def definition(name, *values):
    result = ''
    result += ';' 
    result += name 
    result += '\n'
    for v in values:
        result += ':'
        result += str(v) 
        result += '\n'
    return result
