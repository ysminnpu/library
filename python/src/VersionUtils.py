'''
Created on 19 Jan 2012

@author: seb10184
'''

from datetime import datetime
from subprocess import Popen, PIPE

git_hash = ['git', 'log', '-1', '--pretty=%H']
git_stats = ['git', 'diff', '--numstat']

def get_git_version():
    return Popen(git_hash, stdout=PIPE).communicate()[0].strip()

def get_git_changes(exclude=None):
    output = Popen(git_stats, stdout=PIPE).communicate()[0].strip()
    additions = 0
    deletions = 0
    files = 0
    for line in output.splitlines():
        (a,d,f) = line.split(None, 2)
        if not exclude or not any((f.startswith(x) for x in exclude)):
            try:
                additions += int(a)
                deletions += int(d)
            except ValueError:
                pass
            files += 1
    if files == 0:
        return 'No changes'
    return '%s files changed, %s insertions(+), %s deletions(-)' % (files, additions, deletions)

def get_db_version(cursor):
    if not cursor:
        return 'N/A'
    cursor.execute('SELECT MAX(change_number) AS version FROM changelog')
    return str(cursor.fetchone()['version'])

def write_metadata(out_file, source, cursor=None, exclude=None):
    out_file.write('\t'.join(['#', 'Attributes', '6']) + '\n')
    out_file.write('\t'.join(['#', 'Source', source]) + '\n')
    out_file.write('\t'.join(['#', 'Timestamp', datetime.now().isoformat()]) + '\n')
    out_file.write('\t'.join(['#', 'Version', get_git_version()]) + '\n')
    out_file.write('\t'.join(['#', 'Changes', get_git_changes(exclude)]) + '\n')
    out_file.write('\t'.join(['#', 'Database', get_db_version(cursor)]) + '\n')
            