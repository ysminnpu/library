'''
See LICENCE_BSD for licensing information

@author: spd
'''

import random, sys
import ConfigParser
import os

def seed_random(config=None, seed=None):
    if config and not seed:
        seed = config.get('sample.seed')
    if not seed or not int(seed):
        seed = random.randint(0, sys.maxint)
    random.seed(int(seed))
    
    return seed

def get_config(path, section):
    config = parse_config(path)
    return dict(config.items(section))

def parse_config(path):
    path = os.path.abspath(path)
    config = ConfigParser.SafeConfigParser()
    config.optionxform = str
    if not config.read(path):
        raise IOError('Could not read ' + path)
    return config
